package main

import (
	"github.com/emicklei/go-restful"
	"net/http"
	"HalloBeer/Services"
	"HalloBeer/Logging"
)

func main() {
	Logging.InitLogger()
	Services.MigrateDb()
	InitRest()
}

func InitRest()  {
	restful.Add(Services.NewBeerService())
	restful.Add(Services.NewStyleService())
	http.ListenAndServe(":8080", nil)
}



