package Logging

import(
	log "github.com/sirupsen/logrus"
	"encoding/json"
)

func InitLogger() {
	log.SetLevel(log.DebugLevel)
}

func GetLogger() (*log.Logger) {
	return log.StandardLogger()
}

func PrintObj(obj interface{}) string {
	data, err := json.Marshal(obj)
	if err != nil {
		return err.Error()
	}
	return string(data)
}