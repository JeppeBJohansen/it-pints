package Services

import "HalloBeer/Repositories"

func MigrateDb() {
	Repositories.MigrateStyleFamily()
	Repositories.MigrateStyle()
	Repositories.MigrateBeer()
	Repositories.MigrateRecipe()
	Repositories.MigratePhase()
	Repositories.MigrateIngredient()
	Repositories.MigrateMisc()
	Repositories.MigrateHop()
	Repositories.MigrateMalt()
}