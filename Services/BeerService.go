package Services

import (
	"github.com/emicklei/go-restful"
	"HalloBeer/Models"
	"HalloBeer/Repositories"
	"net/http"
)

func NewBeerService() *restful.WebService {
	ws := new(restful.WebService)
	ws.Path("/beer").Consumes(restful.MIME_JSON).Produces(restful.MIME_JSON)
	ws.Route(ws.GET("/").To(GetBeers).Writes([]Models.Beer{}))
	ws.Route(ws.POST("/").To(CreateBeer))
	return ws
}

func GetBeers(request *restful.Request, response *restful.Response){
	response.WriteEntity(Repositories.GetAllBeers())
}

func CreateBeer(request *restful.Request, response *restful.Response){
	var beer Models.Beer

	err := request.ReadEntity(&beer)
	if err != nil{
		response.WriteError(http.StatusBadRequest, err)
		return
	}

	createdId, dbErr := Repositories.AddBeer(beer)
	if dbErr != nil {
		response.WriteError(http.StatusInternalServerError, dbErr)
		return
	}

	response.WriteEntity(createdId)
}
