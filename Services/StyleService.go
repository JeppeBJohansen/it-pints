package Services

import (
	"github.com/emicklei/go-restful"
	"HalloBeer/Models"
	"HalloBeer/Repositories"
	"net/http"
)

func NewStyleService() *restful.WebService {
	ws := new(restful.WebService)
	ws.Path("/style").Consumes(restful.MIME_JSON).Produces(restful.MIME_JSON)
	ws.Route(ws.GET("/").To(GetStyles).Writes([]Models.Style{}))
	ws.Route(ws.POST("/").To(CreateStyle))
	return ws
}

func GetStyles(request *restful.Request, response *restful.Response){
	response.WriteEntity(Repositories.GetAllStyles())
}

func CreateStyle(request *restful.Request, response *restful.Response){
	var style Models.Style
	err := request.ReadEntity(&style)
	if err != nil{
		response.WriteError(http.StatusBadRequest, err)
		return
	}

	createdId, dbErr := Repositories.AddStyle(style)
	if dbErr != nil {
		response.WriteError(http.StatusInternalServerError, dbErr)
		return
	}

	response.WriteEntity(createdId)
}
