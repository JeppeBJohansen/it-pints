package Repositories

import (
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"HalloBeer/Models"
)

var maltRepoInstance *MaltRepository
var maltRepoOnce sync.Once

type MaltRepository struct {
	*Repository
}

func (repository *MaltRepository) GetMalts() []Models.Malt {
	var malts []Models.Malt
	repository.db.Find(&malts)
	return malts
}

func GetMaltRepoInstance() *MaltRepository {
	maltRepoOnce.Do(func() {
		maltRepoInstance = &MaltRepository{
			NewRepository(),
		}
	})
	return maltRepoInstance
}

func GetAllMalts() []Models.Malt {
	return GetMaltRepoInstance().GetMalts()
}

func MigrateMalt() {
	repo := GetMaltRepoInstance()
	if repo.db.HasTable(&Models.Malt{}) == false {
		repo.db.CreateTable(&Models.Malt{})
	}
	repo.db.AutoMigrate(&Models.Malt{})
}
