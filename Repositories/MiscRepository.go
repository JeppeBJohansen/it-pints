package Repositories

import (
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"HalloBeer/Models"
)

var miscRepoInstance *MiscRepository
var miscRepoOnce sync.Once

type MiscRepository struct {
	*Repository
}

func (repository *MiscRepository) GetMiscs() []Models.Misc {
	var miscs []Models.Misc
	repository.db.Find(&miscs)
	return miscs
}

func GetMiscRepoInstance() *MiscRepository {
	miscRepoOnce.Do(func() {
		miscRepoInstance = &MiscRepository{
			NewRepository(),
		}
	})
	return miscRepoInstance
}

func GetAllMiscs() []Models.Misc {
	return GetMiscRepoInstance().GetMiscs()
}

func MigrateMisc() {
	repo := GetMiscRepoInstance()
	if repo.db.HasTable(&Models.Misc{}) == false {
		repo.db.CreateTable(&Models.Misc{})
	}
	repo.db.AutoMigrate(&Models.Misc{})
}
