package Repositories

import (
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"HalloBeer/Models"
)

var hopRepoInstance *HopRepository
var hopRepoOnce sync.Once

type HopRepository struct {
	*Repository
}

func (repository *HopRepository) GetHops() []Models.Hop {
	var hops []Models.Hop
	repository.db.Find(&hops)
	return hops
}

func GetHopRepoInstance() *HopRepository {
	hopRepoOnce.Do(func() {
		hopRepoInstance = &HopRepository{
			NewRepository(),
		}
	})
	return hopRepoInstance
}

func GetAllHops() []Models.Hop {
	return GetHopRepoInstance().GetHops()
}

func MigrateHop() {
	repo := GetHopRepoInstance()
	if repo.db.HasTable(&Models.Hop{}) == false {
		repo.db.CreateTable(&Models.Hop{})
	}
	repo.db.AutoMigrate(&Models.Hop{})
}
