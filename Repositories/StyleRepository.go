package Repositories

import (
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"HalloBeer/Models"
)

var styleRepoInstance *StyleRepository
var styleRepoOnce sync.Once

type StyleRepository struct {
	*Repository
}

func (repository *StyleRepository) GetStyles() []Models.Style {
	var styles []Models.Style
	repository.db.Find(&styles)
	return styles
}

func (repository *StyleRepository) AddStyle(style Models.Style) (string, error) {
	if err := repository.db.Create(&style).Error; err != nil {
		return "", err
	}
	return style.ID, nil
}

func GetStyleRepoInstance() *StyleRepository {
	styleRepoOnce.Do(func() {
		styleRepoInstance = &StyleRepository{
			NewRepository(),
		}
	})
	return styleRepoInstance
}

func GetAllStyles() []Models.Style {
	return GetStyleRepoInstance().GetStyles()
}

func AddStyle(style Models.Style) (string, error) {
	return GetStyleRepoInstance().AddStyle(style)
}

func MigrateStyle() {
	repo := GetStyleRepoInstance()
	if repo.db.HasTable(&Models.Style{}) == false {
		repo.db.CreateTable(&Models.Style{})
	}
	repo.db.Model(&Models.Style{}).AddForeignKey("style_family_id", "style_families(id)", "SET NULL", "CASCADE")
	repo.db.AutoMigrate(&Models.Style{})
}
