package Repositories

import (
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"HalloBeer/Models"
	"HalloBeer/Logging"
)

var phaseRepoInstance *PhaseRepository
var phaseRepoOnce sync.Once

type PhaseRepository struct {
	*Repository
}

func (repository *PhaseRepository) GetPhases() []Models.Phase {
	var phases []Models.Phase
	repository.db.Find(&phases)
	return phases
}

func (repository *PhaseRepository) AddPhase(phase Models.Phase) (string, error) {
	if err := repository.db.Create(&phase).Error; err != nil {
		return "", err
	}
	return phase.ID, nil
}


func GetPhaseRepoInstance() *PhaseRepository {
	phaseRepoOnce.Do(func() {
		phaseRepoInstance = &PhaseRepository{
			NewRepository(),
		}
	})
	return phaseRepoInstance
}

func GetAllPhases() []Models.Phase {
	return GetPhaseRepoInstance().GetPhases()
}

func AddPhase(phase Models.Phase) (string, error) {
	return GetPhaseRepoInstance().AddPhase(phase)
}

func MigratePhase() {
	repo := GetPhaseRepoInstance()
	if repo.db.HasTable(&Models.Phase{}) == false {
		repo.db.CreateTable(&Models.Phase{})
	}
	repo.db.AutoMigrate(&Models.Phase{})

	SyncPhases()
}

func SyncPhases() {
	repo := GetPhaseRepoInstance()
	var phase Models.Phase
	repo.db.Where(&Models.Phase{Name: "mash"}).First(&phase)
	if phase.ID == "" {
		Logging.GetLogger().Info("Syncing phases")
		phase = Models.Phase{Name:"mash"}
		repo.db.Create(&phase)
		phase = Models.Phase{Name:"boil"}
		repo.db.Create(&phase)
		phase = Models.Phase{Name:"flameout"}
		repo.db.Create(&phase)
		phase = Models.Phase{Name:"whirlpool"}
		repo.db.Create(&phase)
		phase = Models.Phase{Name:"dryhop"}
		repo.db.Create(&phase)
	}
}