package Repositories

import (
	"github.com/jinzhu/gorm"
	"fmt"
	"os"
	"sync"
	log "github.com/sirupsen/logrus"
)

type Repository struct {
	db *gorm.DB
}

var dbInstance *gorm.DB
var dbOnce sync.Once

func NewRepository() (*Repository) {
	repo := new(Repository)
	repo.db = GetDbInstance()
	return repo
}

func GetDbInstance() (*gorm.DB) {
	dbOnce.Do(func() {
		db, dbErr := gorm.Open("mysql", "root:password@/itpints?charset=utf8&parseTime=True&loc=Local")
		if dbErr != nil {
			fmt.Fprint(os.Stderr, dbErr)
			os.Exit(1)
		}
		dbInstance = db
		dbInstance.SetLogger(log.StandardLogger())
	})
	return dbInstance
}