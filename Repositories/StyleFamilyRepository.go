package Repositories

import (
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"HalloBeer/Models"
	"HalloBeer/Logging"
)

var styleFamilyRepoInstance *StyleFamilyRepository
var styleFamilyRepoOnce sync.Once

type StyleFamilyRepository struct {
	*Repository
}

func (repository *StyleFamilyRepository) GetStyleFamilies() []Models.StyleFamily {
	var styleFamilies []Models.StyleFamily
	repository.db.Find(&styleFamilies)
	return styleFamilies
}

func (repository *StyleFamilyRepository) AddStyleFamily(styleFamily Models.StyleFamily) (string, error) {
	if err := repository.db.Create(&styleFamily).Error; err != nil {
		return "", err
	}
	return styleFamily.ID, nil
}


func GetStyleFamilyRepoInstance() *StyleFamilyRepository {
	styleFamilyRepoOnce.Do(func() {
		styleFamilyRepoInstance = &StyleFamilyRepository{
			NewRepository(),
		}
	})
	return styleFamilyRepoInstance
}

func GetAllStyleFamilies() []Models.StyleFamily {
	return GetStyleFamilyRepoInstance().GetStyleFamilies()
}

func AddStyleFamily(styleFamily Models.StyleFamily) (string, error) {
	return GetStyleFamilyRepoInstance().AddStyleFamily(styleFamily)
}

func MigrateStyleFamily() {
	repo := GetStyleFamilyRepoInstance()
	if repo.db.HasTable(&Models.StyleFamily{}) == false {
		repo.db.CreateTable(&Models.StyleFamily{})
	}
	repo.db.AutoMigrate(&Models.StyleFamily{})

	SyncStyleFamilies()
}

func SyncStyleFamilies() {
	repo := GetStyleFamilyRepoInstance()
	var styleFamily Models.StyleFamily
	repo.db.Where(&Models.StyleFamily{Name: "hoppy"}).First(&styleFamily)
	if styleFamily.ID == "" {
		Logging.GetLogger().Info("Syncing style families")
		styleFamily = Models.StyleFamily{Name:"hoppy"}
		repo.db.Create(&styleFamily)
		styleFamily = Models.StyleFamily{Name:"malty"}
		repo.db.Create(&styleFamily)
		styleFamily = Models.StyleFamily{Name:"sour"}
		repo.db.Create(&styleFamily)
		styleFamily = Models.StyleFamily{Name:"black"}
		repo.db.Create(&styleFamily)
		styleFamily = Models.StyleFamily{Name:"lager"}
		repo.db.Create(&styleFamily)
		styleFamily = Models.StyleFamily{Name:"other"}
		repo.db.Create(&styleFamily)
	}
}