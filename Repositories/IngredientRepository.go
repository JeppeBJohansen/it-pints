package Repositories

import (
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"HalloBeer/Models"
)

var ingredientRepoInstance *IngredientRepository
var ingredientRepoOnce sync.Once

type IngredientRepository struct {
	*Repository
}

func (repository *IngredientRepository) GetIngredients() []Models.Ingredient {
	var ingredients []Models.Ingredient
	repository.db.Find(&ingredients)
	return ingredients
}

func GetIngredientRepoInstance() *IngredientRepository {
	ingredientRepoOnce.Do(func() {
		ingredientRepoInstance = &IngredientRepository{
			NewRepository(),
		}
	})
	return ingredientRepoInstance
}

func GetAllIngredients() []Models.Ingredient {
	return GetIngredientRepoInstance().GetIngredients()
}

func MigrateIngredient() {
	repo := GetIngredientRepoInstance()
	if repo.db.HasTable(&Models.Ingredient{}) == false {
		repo.db.CreateTable(&Models.Ingredient{})
	}
	repo.db.Model(&Models.Ingredient{}).AddForeignKey("recipe_id", "recipes(id)", "CASCADE", "CASCADE")
	repo.db.Model(&Models.Ingredient{}).AddForeignKey("phase_id", "phases(id)", "SET NULL", "CASCADE")
	repo.db.AutoMigrate(&Models.Ingredient{})
}
