package Repositories

import (
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"HalloBeer/Models"
)

var recipeRepoInstance *RecipeRepository
var recipeRepoOnce sync.Once

type RecipeRepository struct {
	*Repository
}

func (repository *RecipeRepository) GetRecipes() []Models.Recipe {
	var recipes []Models.Recipe
	repository.db.Find(&recipes)
	return recipes
}

func GetRecipeRepoInstance() *RecipeRepository {
	recipeRepoOnce.Do(func() {
		recipeRepoInstance = &RecipeRepository{
			NewRepository(),
		}
	})
	return recipeRepoInstance
}

func GetAllRecipes() []Models.Recipe {
	return GetRecipeRepoInstance().GetRecipes()
}

func MigrateRecipe() {
	repo := GetRecipeRepoInstance()
	if repo.db.HasTable(&Models.Recipe{}) == false {
		repo.db.CreateTable(&Models.Recipe{})
	}
	repo.db.Model(&Models.Recipe{}).AddForeignKey("beer_id", "beers(id)", "CASCADE", "CASCADE")
	repo.db.AutoMigrate(&Models.Recipe{})
}
