package Repositories

import (
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"HalloBeer/Models"
	"HalloBeer/Logging"
)

var beerRepoInstance *BeerRepository
var beerRepoOnce sync.Once

type BeerRepository struct {
	*Repository
}

func (repository *BeerRepository) GetBeers() []Models.Beer {
	var beers []Models.Beer
	repository.db.Find(&beers)
	return beers
}

func (repository *BeerRepository) AddBeer(beer Models.Beer) (string, error) {
	Logging.GetLogger().Debug("Adding beer: ", Logging.PrintObj(beer))
	if err := repository.db.Create(&beer).Error; err != nil {
		return "", err
	}
	return beer.ID, nil
}


func GetBeerRepoInstance() *BeerRepository {
	beerRepoOnce.Do(func() {
		beerRepoInstance = &BeerRepository{
			NewRepository(),
		}
	})
	return beerRepoInstance
}

func GetAllBeers() []Models.Beer {
	return GetBeerRepoInstance().GetBeers()
}

func AddBeer(beer Models.Beer) (string, error) {
	return GetBeerRepoInstance().AddBeer(beer)
}

func MigrateBeer() {
	repo := GetBeerRepoInstance()
	if repo.db.HasTable(&Models.Beer{}) == false {
		repo.db.CreateTable(&Models.Beer{})
	}
	repo.db.Model(&Models.Beer{}).AddForeignKey("style_id", "styles(id)", "SET NULL", "RESTRICT")
	repo.db.AutoMigrate(&Models.Beer{})
}