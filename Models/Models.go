package Models

import (
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"time"
	"github.com/jinzhu/gorm"
	"github.com/google/uuid"
)

type Entity struct{
	ID string `gorm:"size:40"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

type Beer struct{
	Entity
	Name string
	Style Style
	StyleId string `gorm:"size:40"`
	Description string
	Recipe Recipe
}

type Recipe struct {
	Entity
	Instruction string
	StrikeTemp float32
	MashTemp float32
	BoilTime time.Time
	FermentationSchedule []FermentationSchedule
	Ingredients []Ingredient
	BeerId string `gorm:"size:40"`
}

type FermentationSchedule struct{
	time time.Time
	temp float32
}

type Ingredient struct {
	Entity
	Name string
	Description string
	Amount float32
	Unit string
	Phase Phase
	PhaseId string `gorm:"size:40"`
	TimeOfAddition time.Time
	TimeOfSubtraction time.Time
	RecipeId string `gorm:"size:40"`
}

type Phase struct {
	Entity
	Name string
	Ingredients []Ingredient
}

type Hop struct {
	Ingredient
	AlphaAcid string
	Year string
	Origin string
}

type Malt struct {
	Ingredient
	Color string
	Origin string
}

type Misc struct {
	Ingredient
}

type Style struct {
	Entity
	Name string
	Description string
	StyleFamily StyleFamily
	StyleFamilyId string `gorm:"size:40"`
	Beers []Beer
}

type StyleFamily struct {
	Entity
	Name string
	Styles []Style
}

func (entity *Entity) BeforeCreate(scope *gorm.Scope) error {
	scope.SetColumn("ID", uuid.New().String())
	return nil
}
